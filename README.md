# FortuneMOTD
A simple bukkit/spigot plugin that will change the MOTD based on a random choice from a config.

NOTE: Default config insults you for not configuring the plugin.

## Why?
I couldn't find anything that was as simple as this, that was also open-source.

## What License?
GNU GPL v3.

However, the project this is forked from ([CatsMOTD](https://github.com/zImPatrick/CatMOTD)) is not put under a license. I do feel, however, that this project is distinct enough that it is no longer considerable as an extended CatsMOTD.
