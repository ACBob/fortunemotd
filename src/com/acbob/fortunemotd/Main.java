package com.acbob.fortunemotd;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Random;
import java.util.List;
import java.util.Arrays;
import java.util.logging.Logger;

public class Main extends JavaPlugin implements Listener {
	public static FileConfiguration config; /* get config */
	public static Logger logger;

	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);

		config = this.getConfig();
		logger = this.getLogger();

		config.options().copyDefaults( true ); /* Copy default config */
		saveDefaultConfig(); /* Thanks Badhaloninja for pointing this out */

		logger.info("FortuneMOTD Enabled!");
	}
	@Override
	public void onDisable() {}

	@EventHandler
	public void onServerListPing(ServerListPingEvent e) throws Exception {
		e.setMotd(getFortune());
	}
	public String getFortune() {
		Random rand = new Random();

		List<String> fortunes = config.getStringList("fortunes");
		String fortune = fortunes.get(rand.nextInt(fortunes.size()));
		return fortune;
	}
	public String getOffensiveFortune() {
		Random rand = new Random();

		List<String> fortunes = config.getStringList("offensiveFortunes");
		String fortune = fortunes.get(rand.nextInt(fortunes.size()));
		return fortune;
	}

	/* /fortune command */
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (cmd.getName().equalsIgnoreCase("fortune")) {

			if ( !config.getBoolean("enableFortuneCommand") ) {
				sender.sendMessage("/fortune is not enabled on this server!");
				return true;
			}

			if ( sender instanceof Player ) {
				Player player = (Player) sender;
				if ( !player.hasPermission("fortunemotd.fortunecommand") ) {
					return false;
				}

				List<String> cmdArgs = Arrays.asList(args);

				if ( cmdArgs.contains("-o") || cmdArgs.contains("o") ) {
					if ( !config.getBoolean("enableOffensiveFortunes") )
					{
						sender.sendMessage("Offensive fortunes have been disabled on this server.");
						return true;
					}
					sender.sendMessage(getOffensiveFortune());
				}
				else {
					sender.sendMessage(getFortune());
				}

				return true;
			}
		}

		return false;
	}

}
